using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JetPropulsion : MonoBehaviour
{
 
    public void RotateShip(float degrees)
    {
        if (transform.parent)
        {
            //We are docked,  no rotating here. 
            return;
        }

        GetComponent<Rigidbody2D>().AddTorque(degrees);
    }

    public void ForwardMotion(float speed)
    {
        if (transform.parent)
        {
            //We are docked, no rotating here.
            return;
        }
        GetComponent<Rigidbody2D>().AddForce(transform.up * speed * Time.deltaTime);
        transform.GetChild(0).GetComponent<Animator>().SetBool("isMoving", true);
    }

    public void ApplyBrake()
    {
        Debug.Log("Applying the Brake");
        transform.GetChild(0).GetComponent<Animator>().SetBool("isMoving", false);
        GetComponent<Rigidbody2D>().velocity *= 0.5f;
        
    }
}
