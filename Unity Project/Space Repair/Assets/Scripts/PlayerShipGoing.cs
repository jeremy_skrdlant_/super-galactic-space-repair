﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerShipGoing : MonoBehaviour
{

    public float health = 100;
    public float shields = 100;
    public bool isDocked = false;
    public float maxDockingDistance = 3;
    public float pushOffForce = 20;
    public float vaccineStore = 500;
    public float vaccineSpeed = 0.7f;
    public GameObject status;
    public GameObject healthStatus;
    public GameObject repairStatus;
    public GameObject vaccineStatus;
    public float vaccineUsage = 2;
   
    public GameObject lazer;
    public float lazerSpeed = 100;
    public int worldsRepaired = 0;
    int planetsLeft = 0;

    GameObject currentParent;
    GameObject[] allPlanets;

    private void Start()
    {
        healthStatus.GetComponent<TextMeshProUGUI>().text = "Health - " + health;
        repairStatus.GetComponent<TextMeshProUGUI>().text = "Worlds Repaired   " + worldsRepaired + "/" + planetsLeft;
        vaccineStatus.GetComponent<TextMeshProUGUI>().text = "Glue = " + vaccineStore;
    }
    // Update is called once per frame
    void Update()
    {
        if (isDocked && currentParent.GetComponent<Planet>().isCorrupted)
        {
            if (vaccineStore > 0)
            {
                vaccineStore -= Time.deltaTime * vaccineUsage;
                currentParent.GetComponent<Planet>().healingPower = vaccineSpeed;
                vaccineStatus.GetComponent<TextMeshProUGUI>().text = "Glue = " + vaccineStore;
            }
            else
            {
                currentParent.GetComponent<Planet>().healingPower = 0;
            }
            

        }
    }
    public void addPlanetToHeal()
    {
        planetsLeft += 1;
        repairStatus.GetComponent<TextMeshProUGUI>().text = "Worlds Repaired   " + worldsRepaired + "/" + planetsLeft;
    }

    public void lazerFire() {
        GetComponent<AudioSource>().Play();
        GameObject laz = Instantiate(lazer, transform.position + transform.forward, Quaternion.Euler(transform.localEulerAngles));
        laz.GetComponent<Rigidbody2D>().AddForce(transform.up * lazerSpeed);
        
    }

    public void AddToHealth(float amount)
    {
        health += amount;
        health = Mathf.Min(100, health);
        healthStatus.GetComponent<TextMeshProUGUI>().text = "Health - " + health;
        transform.GetChild(2).gameObject.GetComponent<AudioSource>().Play();
    }

    public void AddToVaccine(float amount)
    {
        vaccineStore += amount;
        vaccineStore = Mathf.Min(1000, vaccineStore);
        vaccineStatus.GetComponent<TextMeshProUGUI>().text = "Glue = " + vaccineStore;
        transform.GetChild(2).gameObject.GetComponent<AudioSource>().Play();
    }

    public void ToggleDock()
    {
         if (isDocked)
        {
            Debug.Log("Pushing Off");
            currentParent = null;
            transform.parent = null ;
            isDocked = false;
            GetComponent<JetPropulsion>().ForwardMotion(-1 * pushOffForce);
            status.GetComponent<TextMeshProUGUI>().text = "Flying";
        }
        else
        {
            //find closest planet and get their distance.
            //if small enough then dock to planet.
            if(allPlanets == null)
            {
                allPlanets = GameObject.FindGameObjectsWithTag("Planet");
            }
            Debug.Log("Now found " + allPlanets.Length + " planets");

            float radiusPlusDistance = transform.localScale.x + maxDockingDistance;

            for(int i = 0; i < allPlanets.Length; i++)
            {
                if (Vector2.Distance(transform.position, allPlanets[i].transform.position) < radiusPlusDistance)
                {
                    currentParent = allPlanets[i];
                    transform.parent = currentParent.transform;
                    if(currentParent.GetComponent<Planet>().isCorrupted && vaccineStore > 0)
                    {
                        currentParent.transform.GetChild(2).gameObject.GetComponent<AudioSource>().Play();
                    }
                    currentParent.GetComponent<Planet>().dockedShip = this.gameObject;
                    GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    GetComponent<Rigidbody2D>().angularVelocity = 0;
                    isDocked = true;
                    status.GetComponent<TextMeshProUGUI>().text = "Docked";
                }
            }
            isDocked = true;

        }

    }

    public void AddToPlanetHealCount()
    {
        worldsRepaired += 1;
        planetsLeft -= 1;
        repairStatus.GetComponent<TextMeshProUGUI>().text = "Worlds Repaired   " + worldsRepaired + "/" + planetsLeft;
        if (planetsLeft == 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
