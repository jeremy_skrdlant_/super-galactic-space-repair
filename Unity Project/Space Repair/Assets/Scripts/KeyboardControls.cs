﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardControls : MonoBehaviour
{
    public GameObject spaceShip;
    public float keyRotationSpeed;
    public float forwardSpeed;
    //r for repair
    public string dockKey = "r";

    // Update is called once per frame
    void Update()
    {
        if (!spaceShip)
        {
            Debug.LogWarning("Missing Spaceship");
        }
        if (Input.GetKey("w") || Input.GetKey("up"))
        {
            //move ship forward
            spaceShip.GetComponent<JetPropulsion>().ForwardMotion(forwardSpeed);
        }
        if (Input.GetKeyUp("w") || Input.GetKeyUp("up"))
        {
            spaceShip.GetComponent<JetPropulsion>().ApplyBrake();
        }
        if (Input.GetKey("a") || Input.GetKey("left"))
        {
            //left turn
            spaceShip.GetComponent<JetPropulsion>().RotateShip(keyRotationSpeed);
        }

        if (Input.GetKey("d") || Input.GetKey("right"))
        {
            //right turn
            spaceShip.GetComponent<JetPropulsion>().RotateShip(keyRotationSpeed * -1);
        }
        if (Input.GetKeyDown("space"))
        {
            spaceShip.GetComponent<PlayerShipGoing>().lazerFire();
        }
        if (Input.GetKey("s") || Input.GetKey("down"))
        {
            spaceShip.GetComponent<JetPropulsion>().ApplyBrake();
        }

        if (Input.GetKeyDown(dockKey))
        {
            spaceShip.GetComponent<PlayerShipGoing>().ToggleDock();
        }
        if (Input.GetKeyDown("left shift") || Input.GetKeyDown("f"))
        {
            spaceShip.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Animator>().SetTrigger("zoom");
        }
        if (Input.GetKeyDown("escape"))
        {
            Debug.Log("Escaped");
            Application.Quit();
        }
    }
}
