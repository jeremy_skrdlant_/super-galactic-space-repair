﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstroidCreator : MonoBehaviour
{
    public int numberOfHealing = 20;
    public int numberOfVaccine = 20;
    public int numberOfDecoy1 = 20;
    public int numberOfDecoy2 = 20;
    public GameObject healingAstroid;
    public GameObject vaccineAstroid;
    public GameObject decoy1Astroid;
    public GameObject decoy2Astroid;
    public float maxSpeedX;
    public float maxSpeedY;

    Board bd;

    void Start()
    {
        bd = GetComponent<Board>();
        GenerateGroup(numberOfHealing, healingAstroid);
        GenerateGroup(numberOfVaccine, vaccineAstroid);
        GenerateGroup(numberOfDecoy1, decoy1Astroid);
        GenerateGroup(numberOfDecoy2, decoy2Astroid);
    }

    void GenerateGroup(int amount, GameObject astroid)
    {
        for (int i = 0; i < amount; i++)
        {
            Vector2 newLocation = GenerateVector2();
            GameObject temp = Instantiate(astroid, newLocation, Quaternion.identity);
            temp.GetComponent<AstroidCollision>().ourboard = bd;
            temp.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(0, maxSpeedX), Random.Range(0, maxSpeedY)));
        }
    }

    

    Vector2 GenerateVector2()
    {
        if (!bd)
        {
            return new Vector2(0, 0);
        }
        float x = Random.Range(bd.min.x, bd.max.x);
        float y = Random.Range(bd.min.y, bd.max.y);

        return new Vector2(x, y);

    }

}
