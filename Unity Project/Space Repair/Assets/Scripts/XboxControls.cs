﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class XboxControls : MonoBehaviour
{
    public GameObject spaceShip;
    public float keyRotationSpeed;
    public float forwardSpeed;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("forward"))
        {
            //move ship forward
            spaceShip.GetComponent<JetPropulsion>().ForwardMotion(forwardSpeed);
        }

        if (Input.GetButton("left"))
        {
            //left turn
            spaceShip.GetComponent<JetPropulsion>().RotateShip(keyRotationSpeed * Time.deltaTime);
            Debug.Log("Going Left");
        }

        if (Input.GetButton("right"))
        {
            //right turn
            spaceShip.GetComponent<JetPropulsion>().RotateShip(keyRotationSpeed * -1 * Time.deltaTime);
        }
        if (Input.GetButton("down"))
        {
            spaceShip.GetComponent<JetPropulsion>().ApplyBrake();
        }
        
       
        if (Input.GetButtonDown("repair"))
        {
            spaceShip.GetComponent<PlayerShipGoing>().ToggleDock();
        }

        if (Input.GetButtonDown("Zoom"))
        {
            spaceShip.transform.GetChild(0).GetChild(0).gameObject.GetComponent<Animator>().SetTrigger("zoom");
 
        }
        if (Input.GetButtonDown("restart"))
        {
            SceneManager.LoadScene("level 1");
        }
        if (Input.GetButtonDown("fire"))
        {
            spaceShip.GetComponent<PlayerShipGoing>().lazerFire();
        }
    }
}
