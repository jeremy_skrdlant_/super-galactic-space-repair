﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetGenerator : MonoBehaviour
{
    public int numberOfPlanets = 20;
    public List<GameObject> planetsList;
    public float minSize = 1;
    public float maxSize = 5;
    public float minDistanceToOtherPlanet = 7;
    public float percentageInfectedPlanets = 0.5f;
    public GameObject ship;

    List<Vector2> allLocations = new List<Vector2>();
    Board bd;

    void Start()
    {
        bd = GetComponent<Board>();

        if (PlayerPrefs.GetInt("difficulty") == 2)
        {
            percentageInfectedPlanets += 0.1f;

        }
        if (PlayerPrefs.GetInt("difficulty") == 3)
        {
            percentageInfectedPlanets += 0.3f;
        }

        int index = 0;
        while (index < numberOfPlanets)
        {
            Vector2 temp = GenerateVector2();
            if (index == 0){
                allLocations.Add(temp);
                index++;
            }
            else
            {
                if (SatisfiesMinDistance(temp))
                {
                    allLocations.Add(temp);
                    index++;
                }
            }

        }
        SpawnAllPlanets();
    }

    void SpawnAllPlanets()
    {
        for(int i = 0; i < allLocations.Count; i++)
        {
            Vector2 position = allLocations[i];
            float size = Random.Range(minSize, maxSize);
            int planetIndex = Random.Range(0, planetsList.Count);
            GameObject newPlanet = Instantiate(planetsList[planetIndex], position, Quaternion.identity);
            newPlanet.transform.localScale = new Vector3(size, size, 1);
            if (i < numberOfPlanets * percentageInfectedPlanets)
            {
                newPlanet.GetComponent<Planet>().isCorrupted = true;
                ship.GetComponent<PlayerShipGoing>().addPlanetToHeal();

            }
        }
    }

    bool SatisfiesMinDistance(Vector2 test)
    {
        for(int i = 0; i < allLocations.Count; i++)
        {
            if (Vector2.Distance(test, allLocations[i]) <= minDistanceToOtherPlanet )
            {
                return false;
            }
        }
        return true;
    }

    Vector2 GenerateVector2()
    {
        if (!bd)
        {
            return new Vector2(0, 0);
        }
        float x = Random.Range(bd.min.x, bd.max.x);
        float y = Random.Range(bd.min.y, bd.max.y);

        return new Vector2(x, y);

    }

}
