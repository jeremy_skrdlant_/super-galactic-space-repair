﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstroidCollision : MonoBehaviour
{
    public Board ourboard;
    public string ourType;


    // Update is called once per frame
    void Update()
    {
        if (transform.position.x > ourboard.max.x ||
            transform.position.y > ourboard.max.y ||
            transform.position.x < ourboard.min.x ||
            transform.position.y < ourboard.min.y)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Planet")
        {
            Destroy(this.gameObject);
        }
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.transform.GetChild(0).gameObject.GetComponent<Animator>().SetTrigger("crash");
        }

        if (collision.gameObject.tag == "Lazer")
        {
            Destroy(collision.gameObject);
            GameObject rock = transform.GetChild(0).gameObject;
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            //check the type of item.
            if (ourType == "decoy")
            {
                rock.GetComponent<Animator>().SetTrigger("Rockie");
            }

            if (ourType == "health")
            {
                rock.GetComponent<Animator>().SetTrigger("Emerald");
                Debug.Log("Destroying");
            }
            if (ourType == "Vaccine")
            {
                rock.GetComponent<Animator>().SetTrigger("BlueRockDestroyed");
                Debug.Log("Destroying the Rock");
            }
        }
    }


}
