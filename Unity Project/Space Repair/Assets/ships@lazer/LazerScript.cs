﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerScript : MonoBehaviour
{
    public float maxLifetime = 30;
    float createdAt = 0; 
    // Start is called before the first frame update
    void Start()
    {
        createdAt = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > createdAt + maxLifetime)
        {
            Debug.Log("Destroying my own lazer");
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Planet")
        {
            Destroy(this.gameObject);
        }

    }
}
