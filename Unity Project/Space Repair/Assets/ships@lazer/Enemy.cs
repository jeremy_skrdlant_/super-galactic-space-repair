﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Lazer")
        {
            Destroy(collision.gameObject);
            GetComponent<AudioSource>().Play();
            transform.GetChild(0).gameObject.GetComponent<Animator>().SetTrigger("killed");
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
    }
}
