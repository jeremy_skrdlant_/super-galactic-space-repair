﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChooser : MonoBehaviour
{
    public List<Sprite> availablePlanetSprites;
    // Start is called before the first frame update
    void Start()
    {
        int randomChoice = Random.Range(0, availablePlanetSprites.Count);
        GetComponent<SpriteRenderer>().sprite = availablePlanetSprites[randomChoice];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
