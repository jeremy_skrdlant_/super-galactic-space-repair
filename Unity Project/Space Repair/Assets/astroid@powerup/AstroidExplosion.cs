﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstroidExplosion : MonoBehaviour
{
    public float healthAmount = 20;

 

    public void CollectHealth()
    {
        GameObject player = GameObject.Find("MainPlayer");
        player.GetComponent<PlayerShipGoing>().AddToHealth(healthAmount);
    }

    public void CollectVaccine()
    {
        GameObject player = GameObject.Find("MainPlayer");
        player.GetComponent<PlayerShipGoing>().AddToVaccine(50);
    }

    public void FinishExplosion()
    {
        Debug.Log("Destroying the parent");
        Destroy(transform.parent.gameObject);
    }
}
