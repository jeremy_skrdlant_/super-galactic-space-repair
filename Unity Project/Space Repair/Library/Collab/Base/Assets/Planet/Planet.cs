﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Planet : MonoBehaviour
{
    GameObject corruptionLayer;
    public float corruptionAmount = 0;
    public bool numberTransitionEnabled = true;
    public bool isCorrupted = false;
    public float corruptionRateSeconds = 1;
    float previousTime = 0;
    public float corruptionRateAmount = 0.05f;
    public float healingPower = 0.0f;
    public GameObject dockedShip;
    public float enemySpawnRate = 30;
    public float lastEnemySpawn = 0;
    public GameObject enemyShip;

    void Start()
    {
        corruptionLayer = transform.GetChild(0).gameObject;
        Color tmp = corruptionLayer.GetComponent<SpriteRenderer>().color;
        tmp.a = 0;
        corruptionLayer.GetComponent<SpriteRenderer>().color = tmp;
    }

    void Update()
    {
        if (numberTransitionEnabled)
        {
            TestEnabled();
        }

        if (isCorrupted)
        {
            if (previousTime + corruptionRateSeconds < Time.time)
            {
                previousTime = Time.time;
                corruptionAmount += corruptionRateAmount - healingPower;
                corruptionAmount = Mathf.Min(corruptionAmount, 1);
                if (corruptionAmount <= 0)
                {
                    Debug.Log("You are Repaired");
                    isCorrupted = false;
                    corruptionAmount = 0f;
                    healingPower = 0f;
                    transform.GetChild(3).gameObject.GetComponent<AudioSource>().Play();
                    dockedShip.GetComponent<PlayerShipGoing>().AddToPlanetHealCount();
                }
                UpdateCorruptionLayer();
            }
        }
        if (lastEnemySpawn + enemySpawnRate < Time.time)
        {
            lastEnemySpawn = Time.time;
            Instantiate(enemyShip, transform.position, Quaternion.identity);
        }
    }

    void TestEnabled()
    {
        if (Input.GetKeyDown("1"))
        {
            corruptionAmount = 0.1f;
            UpdateCorruptionLayer();
        }

        if (Input.GetKeyDown("2"))
        {
            corruptionAmount = 0.2f;
            UpdateCorruptionLayer();
        }

        if (Input.GetKeyDown("3"))
        {
            corruptionAmount = 0.3f;
            UpdateCorruptionLayer();
        }

        if (Input.GetKeyDown("4"))
        {
            corruptionAmount = 0.4f;
            UpdateCorruptionLayer();
        }
        if (Input.GetKeyDown("5"))
        {
            corruptionAmount = 0.5f;
            UpdateCorruptionLayer();
        }
        if (Input.GetKeyDown("6"))
        { 
            corruptionAmount = 0.6f;
            UpdateCorruptionLayer();
        }
        if (Input.GetKeyDown("7"))
        {
            corruptionAmount = 0.7f;
            UpdateCorruptionLayer();
        }
        if (Input.GetKeyDown("8"))
        {
            corruptionAmount = 0.8f;
            UpdateCorruptionLayer();
        }
        if (Input.GetKeyDown("9"))
        {
            corruptionAmount = 1f;
            UpdateCorruptionLayer();
        }
    }

    void UpdateCorruptionLayer()
    {
        Color tmp = corruptionLayer.GetComponent<SpriteRenderer>().color;
        tmp.a = corruptionAmount;
        corruptionLayer.GetComponent<SpriteRenderer>().color = tmp;
        GetComponent<Animator>().SetFloat("corruptionLevel", corruptionAmount);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Collided with " + collision.gameObject.tag);
        if (collision.gameObject.tag == "Player")
        {
       
            //play collision
            collision.gameObject.transform.GetChild(0).gameObject.GetComponent<Animator>().SetTrigger("crash");
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            collision.gameObject.GetComponent<Rigidbody2D>().angularVelocity = 0;
        }

      
    }

}
